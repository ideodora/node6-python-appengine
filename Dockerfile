# Pull base image
FROM node:6.9.1-onbuild
MAINTAINER ideodora <ideodora@gmail.com>

ENV GAE_SDK_VERSION 121.0.0

# Dependencies for install pycrypto
ENV pycryptDependencies\
    build-essential libssl-dev libffi-dev python-dev
     
# Install system libs
RUN \
    apt-get update && \
    apt-get install -y python-pip && \
    apt-get install -y xvfb && \
    apt-get install -fyqq ${pycryptDependencies}

# Install appengine libs
RUN \
    wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${GAE_SDK_VERSION}-linux-x86_64.tar.gz -P /tmp/ &&\
    mkdir -p /usr/local/google &&\
    tar -zxvf /tmp/google-cloud-sdk-${GAE_SDK_VERSION}-linux-x86_64.tar.gz -C /usr/local/google &&\
    rm -rf /tmp/google-cloud-sdk-${GAE_SDK_VERSION}-linux-x86_64.tar.gz

# Set path
RUN /usr/local/google/google-cloud-sdk/install.sh

RUN /usr/local/google/google-cloud-sdk/bin/gcloud components install app-engine-python

# Define default command.
CMD ["bash"]
